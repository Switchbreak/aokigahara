﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {
	
	public Transform target = null;
	public float smoothTime = 0.3f;
	private Vector3 velocity;
	private Vector3 offset;

	// Use this for initialization
	void Start () {
		offset = transform.position - target.position;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if( target )
		{
			Vector3 targetPosition = target.rotation * offset;
			transform.position = Vector3.Lerp( transform.position, target.position + targetPosition, smoothTime );
			transform.rotation = Quaternion.Slerp( transform.rotation, target.rotation, smoothTime );
		}
	}
}
