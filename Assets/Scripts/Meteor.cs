﻿using UnityEngine;
using System.Collections;

public class Meteor : MonoBehaviour
{
	public Vector3 direction = new Vector3( 25, 0, 25 );
	public float lifetime = 10;
	
	// Update is called once per frame
	void Update ()
	{
		if( lifetime > 0 )
		{
			transform.Translate( direction * Time.deltaTime );
			
			lifetime -= Time.deltaTime;
			if( lifetime <= 0 )
			{
				Destroy( gameObject, 5.0f );
			}
		}
	}
}
