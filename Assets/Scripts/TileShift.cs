﻿using UnityEngine;
using System.Collections.Generic;

public class TileShift : MonoBehaviour
{
	public int TileRows			= 3;
	public int TileColumns		= 3;
	public int ActiveTile		= 4;
	public GameObject[] tiles	= new GameObject[9];
	public Vector2 offset		= new Vector2( 250.0f, 250.0f );
	public Vector2 size			= new Vector2( 500.0f, 500.0f );
	public bool randomized		= false;
	public bool shiftX			= true;
	public bool shiftY			= true;
	
	public float minX = 0;
	public float maxX = 0;
	public float minY = 0;
	public float maxY = 0;

	public bool placeObjects = false;
	public int objectsPerTile = 5;
	public List<GameObject> terrainObjects = new List<GameObject>();

	void Start ()
	{
		if( placeObjects )
		{
			foreach( GameObject tile in tiles )
			{
				PlaceTerrainObjects( tile );
			}
		}
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if( shiftX && (minX == 0 || transform.position.x > minX) && (maxX == 0 || transform.position.x < maxX) )
		{
			if( transform.position.x < tiles[ActiveTile].transform.position.x - offset.x )
				ShiftTiles( 0, -1 );
			else if( transform.position.x > (tiles[ActiveTile].transform.position.x - offset.x + size.x) )
				ShiftTiles( 0, 1 );
		}
		if( shiftY && (minY == 0 || transform.position.z > minY) && (maxY == 0 || transform.position.z < maxY) )
		{
			if( transform.position.z < tiles[ActiveTile].transform.position.z - offset.y )
				ShiftTiles( -1, 0 );
			else if( transform.position.z > (tiles[ActiveTile].transform.position.z - offset.y + size.y) )
				ShiftTiles( 1, 0 );
		}
	}
	
	private void ShiftTiles( int rowShift, int columnShift )
	{
		GameObject[] shiftedTiles = new GameObject[9];
		
		Vector3 origin = tiles[0].transform.position;
		for( int row = 0, i = 0; row < TileRows; row++ )
		{
			for( int column = 0; column < TileColumns; column++, i++ )
			{
				int sourceRow = row + rowShift;
				int sourceColumn = column + columnShift;
				
				if( sourceRow < 0 || sourceRow >= TileRows || sourceColumn < 0 || sourceColumn >= TileColumns )
				{
					if( randomized )
					{
						shiftedTiles[i] = GetRandomTile( sourceRow, sourceColumn, shiftedTiles );
					}
					else
					{
						shiftedTiles[i] = GetLoopedTile( sourceRow, sourceColumn );
					}

					Vector3 shiftedPosition = new Vector3(sourceColumn * size.x, 0, sourceRow * size.y) + origin;
					shiftedTiles[i].transform.position = shiftedPosition;
					
					if( placeObjects )
					{
						PlaceTerrainObjects( shiftedTiles[i] );
					}
				}
				else
				{
					shiftedTiles[i] = tiles[sourceRow * TileRows + sourceColumn];
				}
			}
		}
		
		tiles = shiftedTiles;
	}
	
	private GameObject GetRandomTile( int sourceRow, int sourceColumn, GameObject[] shiftedTiles )
	{
		List<GameObject> sourceTiles = new List<GameObject>();
		
		if( sourceRow < 0 || sourceRow >= TileRows )
		{
			sourceRow = (sourceRow + TileRows) % TileRows;
			for( sourceColumn = 0; sourceColumn < TileColumns; sourceColumn++ )
			{
				GameObject sourceTile = tiles[sourceRow * TileRows + sourceColumn];
				if( System.Array.IndexOf( shiftedTiles, sourceTile ) < 0 )
					sourceTiles.Add( sourceTile );
			}
		}
		else if( sourceColumn < 0 || sourceColumn >= TileColumns )
		{
			sourceColumn = (sourceColumn + TileColumns) % TileColumns;
			for( sourceRow = 0; sourceRow < TileRows; sourceRow++ )
			{
				GameObject sourceTile = tiles[sourceRow * TileRows + sourceColumn];
				if( System.Array.IndexOf( shiftedTiles, sourceTile ) < 0 )
					sourceTiles.Add( sourceTile );
			}

		}
		
		return sourceTiles[Random.Range( 0, sourceTiles.Count )];
	}
	
	private GameObject GetLoopedTile( int sourceRow, int sourceColumn )
	{
		sourceRow = (sourceRow + TileRows) % TileRows;
		sourceColumn = (sourceColumn + TileColumns) % TileColumns;
		
		return tiles[sourceRow * TileRows + sourceColumn];
	}

	private void PlaceTerrainObjects( GameObject tile )
	{
		ClearTerrainObjects( tile );

		GameObject container = new GameObject();
		container.name = "TerrainObjects";
		container.transform.parent = tile.transform;

		for( int i = 0; i < objectsPerTile; i++ )
		{
			int objectIndex = Random.Range( 0, terrainObjects.Count );

			Vector3 position = new Vector3( Random.Range( tile.transform.position.x - offset.x, tile.transform.position.x - offset.x + size.x ),
			                               0,
			                               Random.Range( tile.transform.position.z - offset.y, tile.transform.position.z - offset.y + size.y ) );

			GameObject newObject = (GameObject)Instantiate( terrainObjects[objectIndex], position, Quaternion.Euler( new Vector3( 90, 0, 0 ) ) );
			newObject.transform.parent = container.transform;
		}
	}

	private void ClearTerrainObjects( GameObject tile )
	{
		Transform container = tile.transform.FindChild("TerrainObjects");

		if( container != null )
			Destroy( container.gameObject );
	}
}
