﻿using UnityEngine;

public class MeteorEmitter : MonoBehaviour
{
	public float emitRate = 0.1f;
	public GameObject meteorObject;
	
	private float emitTimer = 0;
	
	// Update is called once per frame
	void Update ()
	{
		emitTimer += Time.deltaTime;
		if( emitTimer >= emitRate )
		{
			EmitMeteor();
			emitTimer = 0;
		}
	}
	
	void EmitMeteor()
	{
		Vector3 position = new Vector3( (Random.value - 0.5f) * transform.localScale.x, (Random.value - 0.5f) * transform.localScale.y, (Random.value - 0.5f) * transform.localScale.z );
		position += (transform.position);
		
		Instantiate( meteorObject, position, Quaternion.identity );
	}
}
