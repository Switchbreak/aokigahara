﻿using UnityEngine;
using System.Collections;

public class SunriseTrigger : MonoBehaviour
{
	public GameObject sun;
	public Light sunlight;
	public Vector3 centerPoint;
	public float maxDistance = 500;
	public float targetRotation = 60;
	public float sunIntensity = 1.0f;

	
	private Vector3 startPosition;
	private float distance = 0;
	
	void Start()
	{
		startPosition = sun.transform.position;
		RenderSettings.skybox.SetFloat("_Blend", distance);
	}
	
	void OnTriggerStay( Collider other )
	{
		float newDistance = (maxDistance - Vector3.Distance( other.transform.position, this.transform.position )) / maxDistance;
		
		if( newDistance > distance )
		{
			distance = newDistance;
			
			RotateToTarget( distance );
			sunlight.intensity = sunIntensity * distance;
			RenderSettings.skybox.SetFloat("_Blend", distance);
		}
	}

	void RotateToTarget( float distance )
	{
		Vector3 rotation = new Vector3( -targetRotation * distance, 0, 0 );
		sun.transform.position = Quaternion.Euler( rotation ) * startPosition;
		sun.transform.eulerAngles = rotation;
	}
}
