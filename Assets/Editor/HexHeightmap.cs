﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class HexHeightmap : MonoBehaviour
{
	private static readonly float	scaleSize = 50;
	private static readonly Vector3	startPosition = new Vector3( 25, 0, 131 );
	private static readonly int		rowCount = 10;
	private static readonly int		columnCount = 10;
	private static readonly int		heightMin = 500;
	private static readonly int		heightMax = 1000;

	[MenuItem ("Terrain/Generate Hex Heightmap")]
	static void GenerateMap()
	{
		GameObject hexObject = Resources.LoadAssetAtPath<GameObject>( "Assets/SeasideCliff/Prefabs/Hexagon.prefab" );
		GameObject capObject = Resources.LoadAssetAtPath<GameObject>( "Assets/SeasideCliff/Prefabs/HexCap.prefab" );
		GameObject waterObject = Resources.LoadAssetAtPath<GameObject>( "Assets/SeasideCliff/Prefabs/HexagonWater.prefab" );
		GameObject treeObject = Resources.LoadAssetAtPath<GameObject>( "Assets/SeasideCliff/Prefabs/HexTree.prefab" );
		if( hexObject == null || capObject == null || waterObject == null || treeObject == null )
		{
			EditorUtility.DisplayDialog( "Object not loaded", "Could not find Hexagon tower prefabs.", "OK" );
			return;
		}

		GameObject hexTerrain = CreateTerrainContainer();

		Vector3 position = startPosition;
		Vector3 size = hexObject.transform.GetChild( 0 ).renderer.bounds.size;

		int totalHexes = rowCount * columnCount;
		int currentHex = 0;

		for( int row = 0; row < rowCount; row++ )
		{
			position.x = startPosition.x + (row % 2) * size.x * 0.5f;

			for( int column = 0; column < columnCount; column++, currentHex++ )
			{
				float hexHeight = Random.Range( heightMin, heightMax );

				position.x += size.x;
				position.y = startPosition.y + hexHeight;

				GameObject tile = CreateTileContainer( hexTerrain, currentHex );
				CreateTower( hexObject, position, hexHeight, tile );

				GameObject hexCap = SelectHexCap( ref position, hexHeight, capObject, treeObject, waterObject );
				CreateHexCap( position, tile, hexCap );

				EditorUtility.DisplayProgressBar( "Generate Hex Heightmap", "Generating...", (float)currentHex / totalHexes );
			}

			position.z += size.z * 0.75f;
		}

		EditorUtility.ClearProgressBar();
	}

	static GameObject CreateTerrainContainer()
	{
		GameObject hexTerrain = new GameObject();
		hexTerrain.name = "Hex Terrain";
		hexTerrain.isStatic = true;
		return hexTerrain;
	}

	static GameObject CreateTileContainer( GameObject hexTerrain, int currentHex )
	{
		GameObject tile = new GameObject();
		tile.name = "Tile" + currentHex.ToString();
		tile.transform.parent = hexTerrain.transform;
		tile.isStatic = true;
		return tile;
	}

	static void CreateTower( GameObject hexObject, Vector3 position, float hexHeight, GameObject parent )
	{
		GameObject newHex = (GameObject)Instantiate( hexObject, position, Quaternion.identity );
		newHex.transform.localScale = new Vector3( scaleSize, hexHeight, scaleSize );
		newHex.name = "Tower";
		newHex.transform.parent = parent.transform;

		Mesh towerMesh = newHex.transform.GetChild( 0 ).GetComponent<MeshFilter>().sharedMesh;
		Vector2[] uvs = towerMesh.uv;
		for( int i = 0; i < towerMesh.vertexCount; i++ )
		{
			if( uvs[i].y > 0 )
				uvs[i].y = hexHeight / 100;
		}

		towerMesh.uv = uvs;
	}

	static GameObject SelectHexCap( ref Vector3 position, float hexHeight, GameObject capObject, GameObject treeObject, GameObject waterObject )
	{
		float capSelection = Random.value;
		if( capSelection > 0.5 )
		{
			position.y += hexHeight - 1;
			return capObject;
		} else if( capSelection > 0.1 )
		{
			position.y += hexHeight - 1;
			return treeObject;
		} else
		{
			position.y += hexHeight - 50;
			return waterObject;
		}
	}

	static void CreateHexCap( Vector3 position, GameObject tile, GameObject selectedCap )
	{
		GameObject newCap = (GameObject)Instantiate( selectedCap );
		
		GrassMesh( newCap );

		newCap.name = "Cap";
		newCap.transform.parent = tile.transform;
		newCap.transform.position = position;
	}

	#region Create Cap
	[MenuItem ("Terrain/Generate Test Cap")]
	static void CreateTestHexCap()
	{
		GameObject capObject = Resources.LoadAssetAtPath<GameObject>( "Assets/SeasideCliff/Prefabs/HexCap.prefab" );

		GameObject newCap = (GameObject)Instantiate( capObject );
		newCap.name = "Test Cap";

		GrassMesh( newCap );
		newCap.transform.position = new Vector3( 100, 100, 100 );
	}

	static void GrassMesh( GameObject newCap )
	{
		List<CombineInstance> combine = new List<CombineInstance>();

		List<GameObject> children = CombineGrassMeshes( newCap, combine );
		CombineMeshes( newCap, combine );
		DestroyObjects( children );

		SetGrassMaterial( newCap );
	}

	static List<GameObject> CombineGrassMeshes( GameObject newCap, List<CombineInstance> combine )
	{
		List<GameObject> children = new List<GameObject>();

		for( int i = 0; i < newCap.transform.childCount; i++ )
		{
			GameObject child = newCap.transform.GetChild( i ).gameObject;
			if( child.name == "Grass" )
			{
				combine.Add( ConvertGrassToBillboard( child ) );
				children.Add( child );
			}
		}

		return children;
	}

	static CombineInstance ConvertGrassToBillboard( GameObject child )
	{
		MeshFilter grassFilter = child.GetComponent<MeshFilter>();

		Mesh grassMesh = grassFilter.sharedMesh;
		ConvertVertices( grassMesh );

		CombineInstance combine = new CombineInstance();
		combine.mesh = grassMesh;
		combine.transform = child.transform.localToWorldMatrix;

		return combine;
	}

	static void ConvertVertices( Mesh grassMesh )
	{
		Vector3[] vertices = new Vector3[grassMesh.vertexCount];
		for( int v = 0; v < grassMesh.vertexCount; v++ )
		{
			vertices[v] = new Vector3( 0f, 0.5f, 0f );
		}
		grassMesh.vertices = vertices;
	}

	static void CombineMeshes( GameObject newCap, List<CombineInstance> combine )
	{
		MeshFilter meshFilter = newCap.AddComponent<MeshFilter>();
		meshFilter.sharedMesh = new Mesh();
		meshFilter.sharedMesh.CombineMeshes( combine.ToArray(), true, true );
	}

	static void DestroyObjects( List<GameObject> children )
	{
		foreach( GameObject child in children )
			DestroyImmediate( child );
	}

	static void SetGrassMaterial( GameObject newCap )
	{
		MeshRenderer meshRenderer = newCap.AddComponent<MeshRenderer>();
		meshRenderer.material = Resources.LoadAssetAtPath<Material>( "Assets/SeasideCliff/Materials/GrassBillboard.mat" );
	}
	#endregion
}
