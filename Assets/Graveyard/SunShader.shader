﻿Shader "Custom/SunShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" { }
	}
	SubShader {
        Pass {
            ZTest Always
            SetTexture [_MainTex] { combine texture }
        }
	} 
}
