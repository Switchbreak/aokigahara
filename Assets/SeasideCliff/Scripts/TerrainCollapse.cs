﻿using UnityEngine;
using System.Collections;

public class TerrainCollapse : MonoBehaviour
{
	public GameObject collapsePrefab;
	public float collapseRate = 1.0f;

	private int currentColumn = 0;
	private float collapseTimer = 0;

	// Use this for initialization
	void Start ()
	{
	}

	// Update is called once per frame
	void Update ()
	{
		collapseTimer += Time.deltaTime;
		if( collapseTimer >= collapseRate )
		{
			collapseTimer = 0;
			Collapse( transform.GetChild( currentColumn++ ).gameObject );
		}
	}

	private void Collapse (GameObject column)
	{
		Destroy( column );

		GameObject newColumn = (GameObject)Instantiate( collapsePrefab, column.transform.position, Quaternion.identity );
		newColumn.transform.localScale = new Vector3( column.transform.localScale.x - 1, column.transform.localScale.z, column.transform.localScale.y - 1 );
	}
}
