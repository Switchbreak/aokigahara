﻿using UnityEngine;
using System.Collections;

public class TimeLapse : MonoBehaviour
{
	public float rotateSpeed = 100;
	public Material skybox;

	void Update ()
	{
		transform.Rotate( new Vector3( rotateSpeed * Time.deltaTime, 0, 0 ) );

		float time = Mathf.Clamp01( (Mathf.DeltaAngle( transform.rotation.eulerAngles.x, 90 ) / 100) - 0.4f );
		skybox.SetFloat("_Blend", time);
	}
}
