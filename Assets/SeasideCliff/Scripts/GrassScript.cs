﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent( typeof(MeshFilter) )]
[RequireComponent( typeof(Renderer) )]
public class GrassScript : MonoBehaviour
{
	void Start()
	{
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;

//		Vector4[] tangents = new Vector4[mesh.vertexCount];
//		tangents[2] = new Vector4( -10, -10, 1, 1 );
//		tangents[3] = new Vector4( 10, -10, 1, 1 );
//		tangents[0] = new Vector4( -10, 0, 1, 1 );
//		tangents[1] = new Vector4( 10, 0, 1, 1 );

		Vector3[] vertices = new Vector3[mesh.vertexCount];
		for( int i = 0; i < mesh.vertexCount; i++ )
		{
			vertices[i] = mesh.vertices[0];
		}

//		mesh.tangents = tangents;
		mesh.vertices = vertices;
	}

	// Update is called once per frame
	void Update ()
	{
		Vector4 cameraPosition = new Vector4( Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z, 0.000001f );
		Vector4 cameraRight = Camera.main.transform.right;
		cameraRight.w = 1.0f;
		Vector4 cameraUp = Camera.main.transform.up;
		cameraUp.w = 1.0f;

		renderer.sharedMaterial.SetVector( "_CameraPosition", cameraPosition );
		renderer.sharedMaterial.SetVector( "_CameraRight", cameraRight );
		renderer.sharedMaterial.SetVector( "_CameraUp", cameraUp );
	}
}
