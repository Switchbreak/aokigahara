﻿using UnityEngine;
using System.Collections;

public class PlaneMovement : MonoBehaviour
{
	public float thrust = 50;
	public float lift = 9.8f;
	public float torque = 20;
	
	// Use this for initialization
	void Start ()
	{
		rigidbody.AddForce( transform.forward * thrust );
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if( Input.GetKey( KeyCode.LeftArrow ) )
		{
			rigidbody.AddRelativeTorque( 0, 0, torque );
		}
		if( Input.GetKey( KeyCode.RightArrow ) )
		{
			rigidbody.AddRelativeTorque( 0, 0, -torque );
		}
		if( Input.GetKey( KeyCode.UpArrow ) )
		{
			rigidbody.AddRelativeTorque( torque, 0, 0 );
		}
		if( Input.GetKey( KeyCode.DownArrow ) )
		{
			rigidbody.AddRelativeTorque( -torque, 0, 0 );
		}
		
		//rigidbody.AddForce( transform.forward * thrust );
		rigidbody.velocity = transform.forward * rigidbody.velocity.magnitude;
		rigidbody.AddForce( transform.up * lift );
	}
}
