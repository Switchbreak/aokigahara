﻿using UnityEngine;
using System.Collections;

public class CullGrass : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		float[] distances = new float[32];
		distances[8] = 1000;

		camera.layerCullDistances = distances;
		camera.layerCullSpherical = true;
	}
}
