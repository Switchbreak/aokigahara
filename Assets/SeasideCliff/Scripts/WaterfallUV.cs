﻿using UnityEngine;
using System.Collections;

public class WaterfallUV : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		Mesh mesh = GetComponent<MeshFilter>().mesh;

		Vector2[] uvs = new Vector2[mesh.vertexCount];

		uvs[0] = mesh.uv[0];
		uvs[1] = mesh.uv[1];
		uvs[2] = mesh.uv[2];
		uvs[3] = mesh.uv[3];

		uvs[1].y = 2;
		uvs[3].y = 2;

		mesh.uv = uvs;
	}
}
