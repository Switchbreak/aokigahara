﻿Shader "Custom/Waterfall" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
		_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
		_BumpMap ("Normal Map", 2D) = "bump" {}
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 400
		
		CGPROGRAM
		#pragma surface surf Lambert alpha
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BumpMap;
		fixed4 _Color;
		half _Shininess;

		struct Input {
			fixed2 uv_MainTex;
			fixed2 uv_BumpMap;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed2 scrolledUV1 = IN.uv_MainTex + fixed2( 0, (fixed)_Time * 2.0f );
			fixed4 c = tex2D (_MainTex, scrolledUV1);
			o.Albedo = c.rgb * _Color.rgb;
			o.Alpha = 0.7; //c.a * _Color.rgb;
			
			fixed2 scrolledUV2 = IN.uv_BumpMap + fixed2( 0, (fixed)_Time * 4.0f );
			fixed3 normal2 = UnpackNormal(tex2D(_BumpMap, scrolledUV2));
			
			o.Normal = UnpackNormal(tex2D(_BumpMap, scrolledUV2));
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
