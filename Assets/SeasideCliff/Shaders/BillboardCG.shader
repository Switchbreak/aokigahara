﻿Shader "Cg  shader for billboards" {
   Properties {
      _MainTex ("Texture Image", 2D) = "white" {}
      _Cutoff ("Alpha Cutoff", Float) = 0.5
   }
   SubShader {
      Pass {   
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		
         CGPROGRAM
 
         #pragma vertex vert  
         #pragma fragment frag 
 
         // User-specified uniforms            
         uniform sampler2D _MainTex;
         uniform float _Cutoff;
 
         struct vertexInput {
            float4 vertex : POSITION;
            float4 tex : TEXCOORD0;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 tex : TEXCOORD0;
         };
 
         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;
 
            output.pos = mul(UNITY_MATRIX_P, 
              mul(UNITY_MATRIX_MV, input.vertex)
              + float4(input.tex.x * 10 - 5, input.tex.y * 10, 0.0, 0.0));
 
            output.tex = input.tex;
 
            return output;
         }
 
         float4 frag(vertexOutput input) : COLOR
         {
            float4 textureColor = tex2D(_MainTex, float2(input.tex.xy));
            if( textureColor.a < _Cutoff )
            {
            	discard;
            }
            return textureColor;
         }
 
         ENDCG
      }
   }
}