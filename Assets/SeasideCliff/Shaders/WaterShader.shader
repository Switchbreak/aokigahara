﻿Shader "Custom/Water" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
		_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
		_BumpMap ("Normal Map", 2D) = "bump" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 400
		
		CGPROGRAM
		#pragma surface surf BlinnPhong
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BumpMap;
		fixed4 _Color;
		half _Shininess;

		struct Input {
			fixed2 uv_MainTex;
			fixed2 uv_BumpMap;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * _Color.rgb;
			o.Alpha = c.a * _Color.rgb;
			o.Gloss = c.a;
			o.Specular = _Shininess;
			
			fixed2 scrolledUV1 = IN.uv_BumpMap + fixed2( (fixed)_Time * 0.5, 0.5 );
			fixed2 scrolledUV2 = IN.uv_BumpMap + fixed2( 0, (fixed)_Time * 0.3 );
			fixed3 normal1 = UnpackNormal(tex2D(_BumpMap, scrolledUV1));
			fixed3 normal2 = UnpackNormal(tex2D(_BumpMap, scrolledUV2));
			
			o.Normal = (normal1 + normal2) * 0.5;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
